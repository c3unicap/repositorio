# Repositório

- Este repositório tem como intuito auxiliar o aluno de ciência da computação fornecendo algumas dicas e implementações para as principais matérias do curso. Como também assuntos extra curriculares.

 - Sintam-se livres para mandar pull request.
 
* <pre> <b>Formato</b>
   
       |–-cadeiras
       |  |--cadeira (ex: compiladores)
       |      |--readme
       |      |--linguagem (ex: Python)              
       |          |-- código-fonte   
       |       
       |--guias
       |  |--tecnologia (ex: springFramework)
       |     |--readme
       |     |--gettingStarted
       |        |--código-fonte</pre>
