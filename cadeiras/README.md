# Repositório

- Este repositório tem como intuito auxiliar o aluno de ciência da computação fornecendo algumas dicas e implementações para as principais matérias de programação do curso como também assuntos extra curriculares.

 - Sintam-se livres para acrescentar conteúdo ao mesmo.
 
* <pre> <b>Formato</b>
   
       |–-cadeiras
       |  |--cadeira (ex: compiladores)
       |      |--readme
       |      |--linguagem (ex: Python)              
       |          |-- código-fonte   
       |       
       |--guias
       |  |--springFramework
       |     |--readme
       |     |--gettingStarted
       |        |--código-fonte</pre>
